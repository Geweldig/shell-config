let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'osyo-manga/vim-over'
Plug 'chriskempson/base16-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-commentary'
Plug 'justinmk/vim-sneak'
if has ('nvim')
    Plug 'tveskag/nvim-blame-line'
    Plug 'neovim/nvim-lspconfig'
    Plug 'hrsh7th/nvim-cmp'
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'saadparwaiz1/cmp_luasnip'
    Plug 'L3MON4D3/LuaSnip'
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
endif
call plug#end()

set nocompatible

cmap w!! w !sudo tee > /dev/null %
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
set nu
set incsearch
set backspace=2
syntax on
set ruler
set mouse=a
filetype plugin indent on
if has ('nvim')
    set guicursor=
    set nofoldenable
endif

"Replace highlighting
let g:over_enable_auto_nohlsearch = 1
let g:over_enable_cmd_window = 1
let g:over_command_line_prompt = ">> "
"End replace highlighting

"Colours
let base16colorspace=256
if !exists('g:colors_name') || g:colors_name != 'base16-unikitty-dark'
  colorscheme base16-unikitty-dark
endif
"End colours

"Spellcheck
au bufnewfile,bufreadpost *.md set filetype=markdown
au bufnewfile,bufreadpost *.txt set filetype=txt
au bufnewfile,bufreadpost *.tex set filetype=tex
set spelllang=en_gb,nl
set spell
"End spellcheck

"Powerline theme
set laststatus=2
let g:airline_powerline_fonts=0
let g:airline_detect_paste=1
let g:airline#extensions#tabline#enabled=1
let g:airline_theme='base16'
"End powerline theme

"Comments
map <silent> // :Commentary<CR>
"End comments

"Git blame
if has ('nvim')
    noremap <silent> <leader>b :ToggleBlameLine<CR>
endif
"End Git blame

"Aliases
:command OR OverCommandLine
:command RTW %s/\s\+$//e
:command DIFF w !diff % -
noremap <Leader>c "+
