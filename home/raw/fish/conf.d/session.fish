set TTY1 (tty)

if [ "$TTY1" = "/dev/tty1" ]
    set -q _JAVA_AWT_WM_NONREPARENTING || set -Ux _JAVA_AWT_WM_NONREPARENTING 1
    if [ "$barmer_wm" = "sway" ]
        set -q XDG_CURRENT_DESKTOP || set -U XDG_CURRENT_DESKTOP sway
        exec sway
    else if [ "$barmer_wm" = "river" ]
        exec river
    end
    echo "use `set -U barmer_wm [\"sway\", \"river\"]` to set the WM to use"
end
