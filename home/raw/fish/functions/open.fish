function open --description 'open a file or folder' --argument file or folder
    xdg-open $argv >/dev/null 2>&1 &
    disown
end
