function fish_prompt
    set -l __last_command_exit_status $status

    if not set -q -g __fish_barmer_functions_defined
        set -g __fish_barmer_functions_defined

        function _git_branch_name
            set -l branch (git symbolic-ref --quiet HEAD 2>/dev/null)
            if set -q branch[1]
                echo (string replace -r '^refs/heads/' '' $branch)
            else
                echo (git rev-parse --short HEAD 2>/dev/null)
            end
        end

        function _is_git_dirty
            echo (git status -s --ignore-submodules=dirty 2>/dev/null | grep -v "^??")
        end

        function _is_git_repo
            type -q git
            or return 1
            git rev-parse --git-dir >/dev/null 2>&1
        end

        function _is_git_untracked
            echo (git status --porcelain 2>/dev/null | grep "^??")
        end

        function _is_git_unpushed
            echo (command git cherry -v 2>/dev/null | wc -l)
        end
    end

    set -l red (set_color red)
    set -l red_bold (set_color -o red)
    set -l normal (set_color normal)
    set -l blue_bold (set_color -o blue)
    set -l blue (set_color blue)
    set -l yellow (set_color -o yellow)
    set -l white (set_color white)
    set -l purple (set_color purple)
    set -l green (set_color -o green)
    set -l bright_cyan (set_color brcyan)

    set -l status_color "$white"
    if test $__last_command_exit_status != 0
        set status_color "$red"
    end

    set -l prefix "$status_color""["
    set -l suffix "$status_color]"

    set -l cwd $blue_bold(prompt_pwd)
    set -l arrow "$blue>"

    if _is_git_repo
        set -l branch $purple(_git_branch_name)
        set repo_info " $branch"

        set -l dirty (_is_git_dirty)
        if test -n "$dirty"
            set -l dirty "$red!"
            set repo_info "$repo_info$dirty"
        end
        set -l untracked (_is_git_untracked)
        if test -n "$untracked"
            set -l untracked "$green?"
            set repo_info "$repo_info$untracked"
        end
        set -l unpushed (_is_git_unpushed)
        if test 0 -ne "$unpushed"
            set -l unpushed "$yellow+"
            set repo_info "$repo_info$unpushed"
        end
    end

    set duration ""
    set -l cmd_dur $CMD_DURATION
    if test $barmer_show_prompt_timer = true && test $cmd_dur
        if ! test $barmer_prompt_timer_limit
            set barmer_prompt_timer_limit 10
        end
        if test $cmd_dur -gt (math "1000 * $barmer_prompt_timer_limit")
            set duration $white(format_time $cmd_dur)
            set duration "$white$duration "
        end
    end

    set private ""
    if set -q __is_private_instance
        set -l private_text "[PRIVATE]"
        set private "$red_bold$private_text "
    end

    set nix_shell ""
    if test -n "$IN_NIX_SHELL" || echo "$PATH" | grep -qc '/nix/store'
        set -l nix_text "λ"
        set nix_shell "$blue_bold$nix_text "
    end


    set -l prompt "$nix_shell$normal$prefix$normal$cwd$normal$suffix$normal$repo_info$normal $private$normal$duration$normal$arrow$normal "

    echo -n $prompt
end
