function reload_pipewire --description 'Reload pipewire'
    systemctl --user daemon-reload
    systemctl --user restart pipewire pipewire-pulse
    systemctl --user status pipewire pipewire-pulse | grep "Active:\|\.service -" | sed 's/^\s*//'
end
