function rtw --description 'remove all trailing whitespace for file' -a file
    sed --in-place 's/[[:space:]]\+$//' $file
end
