function amimullvad -d "Check if you're connected to mullvad"
    curl -s https://am.i.mullvad.net/connected
end
