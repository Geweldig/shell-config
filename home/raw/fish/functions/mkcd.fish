function mkcd -a folder -d "create a folder and cd into it"
    mkdir -p $argv && cd $argv
end
