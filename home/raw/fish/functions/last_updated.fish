function last_updated --description 'check when dependencies were last updated for a flake.lock'
    jq ".nodes | map_values(.locked.lastModified | todate?)"
end
