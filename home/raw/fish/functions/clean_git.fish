function clean_git -a legacy
    if contains -- "$legacy" -l --legacy
        set branch master
    else
        set branch main
    end
    git checkout $branch
    git pull
    git fa
    git branch --merge $branch | grep -Ev "(^\*|$branch)" | xargs --no-run-if-empty git branch -d
end
