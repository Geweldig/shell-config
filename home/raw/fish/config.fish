set --local config_version 4
if test -z $__config_version || test $config_version -gt $__config_version
    set -U fish_greeting
    set -Ux EDITOR nvim
    set -Ux VISUAL nvim
    set -Ux SUDO_EDITOR nvim
    set -Ux BROWSER firefox
    set -U fish_prompt_pwd_dir_length 0

    set -U fish_user_paths $fish_user_paths "/home/bram/.local/bin"
    set -U fish_user_paths $fish_user_paths "/home/bram/.cargo/bin"
    set -U barmer_show_prompt_timer true
    set -U barmer_prompt_timer_limit 10

    set -U fish_features stderr-nocaret
    set -U __config_version $config_version

    # Theme
    set -U fish_color_autosuggestion 666369
    set -U fish_color_cancel -r
    set -U fish_color_command green
    set -U fish_color_comment 666369
    set -U fish_color_cwd green
    set -U fish_color_cwd_root red
    set -U fish_color_end brblack
    set -U fish_color_error red
    set -U fish_color_escape yellow
    set -U fish_color_history_current --bold
    set -U fish_color_host normal
    set -U fish_color_host_remote yellow
    set -U fish_color_match --background=brblue
    set -U fish_color_normal normal
    set -U fish_color_operator blue
    set -U fish_color_param 9f9da2
    set -U fish_color_quote yellow
    set -U fish_color_redirection cyan
    set -U fish_color_search_match bryellow --background=666369
    set -U fish_color_selection white --bold --background=666369
    set -U fish_color_status red
    set -U fish_color_user brgreen
    set -U fish_color_valid_path --underline
    set -U fish_pager_color_completion normal
    set -U fish_pager_color_description yellow --dim
    set -U fish_pager_color_prefix white --bold
    set -U fish_pager_color_progress brwhite --background=cyan
end

if test -n (command -s keychain)
    eval (SHELL=fish keychain --eval --quiet --agents gpg,ssh (find $HOME/.ssh -maxdepth 1 -type f ! -name "*.pub" ! -name "known_hosts" ! -name "config" -exec basename {} ';') --nogui)
    set -x GPG_TTY (tty)
end

if test (history | wc -l) -eq 0
    set __is_private_instance
end

set -x GID (id -g)
alias ls="eza"
alias ll="eza -aal"
alias cat=bat
