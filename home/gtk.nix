{ ... }:
{
  home-manager.users.bram = {
    gtk = {
      enable = true;
      cursorTheme = {
        name = "Adwaita";
        size = 0;
      };
      font = {
        name = "Cantarell";
        size = 11;
      };
      iconTheme = {
        name = "Adwaita";
      };
      theme = {
        name = "deepin-dark";
      };
      gtk2.extraConfig = ''
        gtk-toolbar-style=GTK_TOOLBAR_BOTH_HORIZ
        gtk-toolbar-icon-size=GTK_ICON_SIZE_LARGE_TOOLBAR
        gtk-button-images=0
        gtk-menu-images=0
        gtk-enable-event-sounds=1
        gtk-enable-input-feedback-sounds=1
        gtk-xft-antialias=1
        gtk-xft-hinting=1
        gtk-xft-hintstyle="hintfull"
        gtk-xft-rgba="rgb"
      '';
      gtk3 = {
        bookmarks = [ "file:///tmp" ];
        extraConfig = {
          gtk-toolbar-style = "GTK_TOOLBAR_BOTH_HORIZ";
          gtk-toolbar-icon-size = "GTK_ICON_SIZE_LARGE_TOOLBAR";
          gtk-button-images = 0;
          gtk-menu-images = 0;
          gtk-enable-event-sounds = 1;
          gtk-enable-input-feedback-sounds = 1;
          gtk-xft-antialias = 1;
          gtk-xft-hinting = 1;
          gtk-xft-hintstyle = "hintfull";
          gtk-xft-rgba = "rgb";
        };
      };
      gtk4 = {
        extraConfig = {
          gtk-toolbar-style = "GTK_TOOLBAR_BOTH_HORIZ";
          gtk-toolbar-icon-size = "GTK_ICON_SIZE_LARGE_TOOLBAR";
          gtk-button-images = 0;
          gtk-menu-images = 0;
          gtk-enable-event-sounds = 1;
          gtk-enable-input-feedback-sounds = 1;
          gtk-xft-antialias = 1;
          gtk-xft-hinting = 1;
          gtk-xft-hintstyle = "hintfull";
          gtk-xft-rgba = "rgb";
          gtk-application-prefer-dark-theme=1;
        };
      };
    };
  };
}
