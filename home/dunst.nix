{ pkgs, ... }:
{
  home-manager.users.bram = {
    xdg.configFile."dunst/dunstrc".text = ''
      [global]
          monitor = 0
          follow = none
          width = 300
          origin = top-right
          offset = 20x22
          scale = 0
          progress_bar = true
          progress_bar_height = 10
          progress_bar_frame_width = 1
          progress_bar_min_width = 150
          progress_bar_max_width = 300
          indicate_hidden = yes
          transparency = 15
          separator_height = 2
          padding = 8
          horizontal_padding = 8
          text_icon_padding = 0
          frame_width = 1
          frame_color = "#D4D5DA"
          separator_color = frame
          sort = yes

          ### Text ###

          font = Lilex Nerd Font Mono 9
          line_height = 0
          markup = full
          format = "<b>%a</b>\n%s\n%b"
          alignment = left
          vertical_alignment = center
          show_age_threshold = 60
          ellipsize = middle
          ignore_newline = no
          word_wrap = true
          ignore_newline = no
          stack_duplicates = true
          hide_duplicate_count = false
          show_indicators = false

          ### Icons ###
          icon_position = left
          min_icon_size = 0
          max_icon_size = 32
          icon_path = ${pkgs.gnome-icon-theme}/share/icons/gnome/16x16/status/:${pkgs.gnome-icon-theme}/share/icons/gnome/16x16/devices/:${pkgs.breeze-icons}/share/icons/breeze-dark/status/:${pkgs.breeze-icons}/share/icons/breeze-dark/devices/

          ### History ###

          sticky_history = false
          history_length = 20

          ### Misc/Advanced ###

          dmenu = /usr/bin/env fuzzel -dmenu -p dunst:
          browser = /usr/bin/env firefox -new-tab
          always_run_script = true
          title = Dunst
          class = Dunst
          ignore_dbusclose = false
          force_xwayland = false
          force_xinerama = false
          corner_radius = 0
          force_xinerama = false

          ### mouse
          mouse_left_click = close_current
          mouse_middle_click = do_action
          mouse_right_click = close_all

      # Experimental features that may or may not work correctly. Do not expect them
      # to have a consistent behaviour across releases.
      [experimental]
          per_monitor_dpi = false

      [urgency_low]
          background = "#222222"
          foreground = "#888888"
          timeout = 3

      [urgency_normal]
          background = "#4A3C4C"
          foreground = "#ffffff"
          timeout = 5

      [urgency_critical]
          background = "#F43C3C"
          foreground = "#ffffff"
          timeout = 0
    '';
  };
}
