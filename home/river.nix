{
  config,
  lib,
  ...
}:
{
  home-manager.users.bram = {
    xdg.configFile = lib.mkIf (config.programs.river.enable) { "kanshi".source = ./raw/kanshi; };
  };
}
