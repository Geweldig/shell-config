{ pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.bram = {
    isNormalUser = true;
    description = "bram";
    extraGroups = [
      "dialout"
      "networkmanager"
      "video"
      "wheel"
      "wireshark"
    ];
    shell = pkgs.fish;
  };

  users.defaultUserShell = pkgs.fish;
}
