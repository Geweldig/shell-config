{ config, pkgs, ... }:
{
  fileSystems = {
    "/".options = [
      "compress=zstd"
      "discard=async"
    ];
    "/boot".options = [ "discard" ];
    "/home".options = [
      "compress=zstd"
      "discard=async"
    ];
    "/nix".options = [
      "compress=zstd"
      "noatime"
      "discard=async"
    ];
    "/var/log".options = [
      "compress=zstd"
      "discard=async"
    ];
    "/var/lib".options = [
      "compress=zstd"
      "discard=async"
    ];
    "/.snapshots".options = [
      "compress=zstd"
      "discard=async"
    ];
  };

  boot.initrd.luks.devices."root_crypt".allowDiscards = true;

  powerManagement = {
    enable = true;
    powertop.enable = true;
  };

  services = {
    thermald.enable = true;
    auto-cpufreq = {
      enable = true;
      settings = {
        charger = {
          governor = "performance";
          energy_performance_preference = "performance";
          turbo = "auto";
        };
        battery = {
          governor = "powersave";
          energy_performance_preference = "power";
          turbo = "auto";
        };
      };
    };
  };

  boot.kernelModules = [
    "cros_ec"
    "cros_ec_lpcs"
    "framework_laptop"
  ];
  boot.extraModulePackages = with config.boot.kernelPackages; [ framework-laptop-kmod ];
  boot.extraModprobeConfig = ''
    options iwlwifi bt_coex_active=0
  '';

  environment.systemPackages = with pkgs; [
    lm_sensors
    unstable.framework-tool
  ];
}
