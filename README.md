# README

- Add your GPG key and email config to ~/.gitconfig.local
- Add a firefox profile called "chat_applications"
- Add wallpaper.png to ~/Pictures
- Symlink flake.nix to /etc/nixos/flake.nix
