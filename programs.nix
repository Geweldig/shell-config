{ pkgs, ... }:
{
  environment = {
    systemPackages = with pkgs; [
      android-file-transfer
      android-tools
      calibre
      dash
      dnsutils
      ed
      fdupes
      file
      gcc
      gimp
      gparted
      grim
      guvcview
      imagemagick
      imv
      inkscape
      jq
      logiops
      lsof
      moreutils
      networkmanagerapplet
      nmap
      pciutils
      pcmanfm
      rename
      skim
      sqlite
      strawberry
      tree
      tty-solitaire
      ungoogled-chromium
      unzip
      usbutils
      util-linux
      vlc
      xsettingsd
      zathura
      zip

      unstable.alacritty
      unstable.asunder
      unstable.bandwhich
      unstable.bat
      unstable.darktable
      unstable.doublecmd
      unstable.eza
      unstable.fd
      unstable.ffmpeg-full
      unstable.fuzzel
      unstable.gallery-dl
      unstable.hyperfine
      unstable.kid3-qt
      unstable.mc
      unstable.nixd
      unstable.nixfmt-rfc-style
      unstable.nix-index
      unstable.prismlauncher
      unstable.pyright
      unstable.python313
      unstable.qbittorrent
      unstable.qdirstat
      unstable.ripgrep
      unstable.shellcheck
      unstable.signal-desktop
      unstable.telegram-desktop
      unstable.tenacity
      unstable.vitetris
      unstable.yt-dlp

      (unstable.mpv.override {
        scripts = with unstable.mpvScripts; [
          mpris
          thumbfast
          uosc
        ];
      })
      (unstable.proxmark3.override { withBlueshark = true; })
    ];

    etc = {
      "logid.cfg".source = ./raw/logid.cfg;
    };
  };

  programs = {
    fish.enable = true;
    git.enable = true;
    htop.enable = true;
    light.enable = true;
    nano.enable = false;

    firefox = {
      enable = true;
      package = pkgs.firefox;
      languagePacks = [
        "en-US"
        "nl"
      ];
    };
    gnupg.agent = {
      enable = true;
      pinentryPackage = pkgs.pinentry-curses;
      enableSSHSupport = true;
    };
    neovim = {
      package = pkgs.unstable.neovim-unwrapped;
      enable = true;
      defaultEditor = true;
      viAlias = true;
      vimAlias = true;
    };
    wireshark = {
      enable = true;
      package = pkgs.wireshark-qt;
    };
  };
}
